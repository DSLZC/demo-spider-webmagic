package com.dslcode.spider.webmagic.yachang;

import com.dslcode.spider.webmagic.DemoSpiderWebmagicApplicationTests;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import us.codecraft.webmagic.Spider;

/**
 * @author dongsilin
 * @date 2019/5/25 15:25 
 *
 */
@Slf4j
public class ArtWorkTest extends DemoSpiderWebmagicApplicationTests {

	@Autowired
	private ArtWorkPageProcessor artWorkPageProcessor;
	@Autowired
	private ArtWorkPipeline artWorkPipeline;

	@Test
	public void test() {
		log.debug(" =========================开始雅昌抓取Java数据===========================");


		Spider
			.create(artWorkPageProcessor)
			.addUrl("https://artso.artron.net/auction/search_auction.php?keyword=&Status=0&ClassCode=&ArtistName=&OrganCode=&listtype=0&order=&EvaluationType=&Estartvalue=&Eendvalue=&Sstartvalue=&Sendvalue=&StartDate=&EndDate=2019-03-31")
			.addPipeline(artWorkPipeline)
			.thread(10)
			.run();
	}
}
