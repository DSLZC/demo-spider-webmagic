package com.dslcode.spider.webmagic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoSpiderWebmagicApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoSpiderWebmagicApplication.class, args);
	}
}
