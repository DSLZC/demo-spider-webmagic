package com.dslcode.spider.webmagic.yachang.db;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author dongsilin
 * @date 2019/5/25 15:10 
 *
 */
@Slf4j
@Service
public class ArtWorkService {

	@Autowired
	private ArtWorkRepository artWorkRepository;

	@Transactional(rollbackFor = Exception.class)
	public void save(ArtWork artWork) {
		artWorkRepository.save(artWork);
	}

	@Transactional(rollbackFor = Exception.class)
	public void save(List<ArtWork> artWorkList) {
		artWorkRepository.saveAll(artWorkList);
	}
}
