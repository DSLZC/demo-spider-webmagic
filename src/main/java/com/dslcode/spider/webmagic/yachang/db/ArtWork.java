package com.dslcode.spider.webmagic.yachang.db;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;

/**
 * @author dongsilin
 * @date 2019/5/25 14:59 
 * 艺术品Entity
 */
@Data
@Entity
@Table(name = "artwork")
@Accessors(chain = true)
public class ArtWork {
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private Long id;

	/** 第三方ID */
	private String thirdId;
	/** LOT号 */
	private String lotNum;
	/** 标题 */
	private String title;
	/** 详情链接 */
	private String httpUrl;
	/** 估价 */
	private String evaluatePrice;
	/** 估价货币 */
	private String evaluatePriceCurrency;
	/** 估价货币图片 */
	private String evaluatePriceCurrencyImg;
	/** 成交价 */
	private String dealPrice;
	/** 估价货币 */
	private String dealPriceCurrency;
	/** 估价货币图片 */
	private String dealPriceCurrencyImg;
	/** 拍卖公司 */
	private String auctionCompanyName;
	/** 拍卖公司官网链接 */
	private String auctionCompanyUrl;
	/** 拍卖日期 */
	private String auctionDate;
	/** 列表页图片链接 */
	private String miniImageUrl;
	/** 详情页图片链接 */
	private String bigImageUrl;

}