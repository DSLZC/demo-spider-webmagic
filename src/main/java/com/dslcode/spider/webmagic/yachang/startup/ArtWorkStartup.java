package com.dslcode.spider.webmagic.yachang.startup;

import com.dslcode.spider.webmagic.yachang.ArtWorkPageProcessor;
import com.dslcode.spider.webmagic.yachang.ArtWorkPipeline;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import us.codecraft.webmagic.Spider;

/**
 * @author dongsilin
 * @date 2019/5/27 17:18 
 *
 */
@Slf4j
@Configuration
public class ArtWorkStartup implements ApplicationListener<ApplicationReadyEvent> {

	@Autowired
	private ArtWorkPageProcessor artWorkPageProcessor;
	@Autowired
	private ArtWorkPipeline artWorkPipeline;

	@Override
	public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
		log.debug(" =========================开始雅昌抓取Java数据===========================");


		Spider
			.create(artWorkPageProcessor)
			.addUrl("https://artso.artron.net/auction/search_auction.php?keyword=&Status=0&ClassCode=&ArtistName=&OrganCode=&listtype=0&order=&EvaluationType=&Estartvalue=&Eendvalue=&Sstartvalue=&Sendvalue=&StartDate=&EndDate=2019-03-31")
			.addPipeline(artWorkPipeline)
			.thread(10)
			.run();

	}
}
