package com.dslcode.spider.webmagic.yachang;

import com.dslcode.spider.webmagic.yachang.db.ArtWork;
import com.dslcode.spider.webmagic.yachang.db.ArtWorkService;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

/**
 * Created by dongsilin on 2017/6/1.
 */
@Slf4j
@Component
public class ArtWorkPipeline implements Pipeline {

    @Autowired
    private ArtWorkService artWorkService;

	private int THREAD_SIZE = Runtime.getRuntime().availableProcessors();
	private ExecutorService EXECUTOR_SERVICE = new ThreadPoolExecutor(
		THREAD_SIZE,
		THREAD_SIZE * 2,
		5L,
		TimeUnit.SECONDS,
		new LinkedBlockingQueue<>(),
		new ThreadFactoryBuilder().setNameFormat("my-thread-pool-%d").build()
	);
    private volatile Map<String, ArtWork> dataCenter = new ConcurrentHashMap(1000);

    @Override
    public void process(ResultItems resultItems, Task task) {
    	EXECUTOR_SERVICE.submit(() -> {
			if(resultItems.getAll().containsKey("artWorkList")) {
				List<ArtWork> artWorkList = resultItems.get("artWorkList");
				artWorkList.forEach(artWork -> {
					ArtWork oldArtWork = dataCenter.put(artWork.getThirdId(), artWork);
					if (oldArtWork != null) {
						artWorkService.save(artWork.setBigImageUrl(oldArtWork.getBigImageUrl()));
						dataCenter.remove(artWork.getThirdId());
					}
				});
			}
			if(resultItems.getAll().containsKey("artWork")) {
				ArtWork artWork = resultItems.get("artWork");
				ArtWork oldArtWork = dataCenter.put(artWork.getThirdId(), artWork);
				if (oldArtWork != null) {
					artWorkService.save(oldArtWork.setBigImageUrl(artWork.getBigImageUrl()));
					dataCenter.remove(artWork.getThirdId());
				}
			}
		});
    }

}
