package com.dslcode.spider.webmagic.yachang.db;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author dongsilin
 * @date 2019/5/25 15:09 
 *
 */
public interface ArtWorkRepository extends JpaRepository<ArtWork, Long> {
}
